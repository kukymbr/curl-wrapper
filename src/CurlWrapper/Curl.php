<?php

namespace Kukymbr\CurlWrapper;

/**
 * cURL php functions OOP wrapper
 */
class Curl
{
	/**
	 * cURL resource
	 * @var resource
	 */
	protected $_handler;

	/**
	 * Core_Curl constructor.
	 *
	 * @param array|null $options
	 * @throws Exception
	 */
	public function __construct(array $options = null)
	{
		if (!function_exists('curl_init')) {
			throw new Exception('cURL functions are not available');
		}

		$options = array_replace(
			$this->_getDefaultOptions(),
			($options === null ? [] : $options)
		);

		$this->_handler = curl_init();

		$this->setOptArray($options);
	}

	/**
	 * Close handler on destruct
	 */
	public function __destruct()
	{
		curl_close($this->_handler);
	}

	/**
	 * Set option
	 *
	 * @param int $option
	 * @param $value
	 * @return bool
	 */
	public function setOpt(int $option, $value) :bool
	{
		return curl_setopt($this->_handler, $option, $value);
	}

	/**
	 * Set options array
	 *
	 * @param array $options
	 * @return bool
	 */
	public function setOptArray(array $options) :bool
	{
		return curl_setopt_array($this->_handler, $options);
	}

	/**
	 * Set URL
	 *
	 * @param string $url
	 * @param array|null $getParams
	 * @return bool
	 */
	public function setUrl(string $url, array $getParams = null) :bool
	{
		if ($getParams !== null) {
			$params = http_build_query($getParams);
			if ($params) {
			    $url .= (strpos($url, '?') === false ? '?' : '&') . $params;
            }
		}

		return $this->setOpt(CURLOPT_URL, $url);
	}

	/**
	 * Execute request
	 *
	 * @param string|null $url
	 * @param array|null $getParams
	 * @return mixed
	 */
	public function exec(string $url = null, array $getParams = null)
	{
		if ($url !== null) {
			$this->setUrl($url, $getParams);
		}

		return curl_exec($this->_handler);
	}

	/**
	 * Get last request error data
	 *
	 * @param null $errorText
	 * @return int
	 */
	public function getError(&$errorText = null) :int
	{
		$err = curl_errno($this->_handler);

		if ($err) {
			$errorText = curl_error($this->_handler);
		}

		return $err;
	}

	/**
	 * Get last request info
	 *
	 * @param int|null $option
	 * @return mixed
	 */
	public function getInfo(int $option = null)
	{
		return curl_getinfo($this->_handler, $option);
	}

	/**
	 * Get last request response code
	 *
	 * @return int
	 */
	public function getResponseCode() :int
	{
		return (int)$this->getInfo(CURLINFO_HTTP_CODE);
	}

	/**
	 * Non-implemented methods fallback.
	 * Converts $curl->someMethod() to curl_some_method() function call.
	 *
	 * @param $name
	 * @param $arguments
	 * @return mixed
	 * @throws Exception
	 */
	public function __call($name, $arguments)
	{
		$func = $name;
		if (preg_match_all('/((?:^|[A-Z])[a-z]+)/', $name, $matches)) {
			$func = implode('_', $matches[0]);
		}
		$func = 'curl_' . $func;

		if (!function_exists($func)) {
			throw new Exception(
				'Method ' . $name . ' does not exist in ' . __CLASS__ . ', function ' . $func . ' is not found too'
			);
		}

		if (!$arguments) {
			$arguments = [];
		}
		array_unshift($arguments, $this->_handler);

		return call_user_func_array($func, $arguments);
	}

	/**
	 * Get default cURL options.
	 *
	 * @return array
	 */
	protected function _getDefaultOptions() :array
	{
		return [];
	}
}
