# cURL Wrapper

cURL php functions OOP wrapper.

## Requirements

Requires PHP 7.0+.

## Installation

The supported way of installing Curl is via Composer.

```bash
$ composer require kukymbr/curl-wrapper
```

## Usage

```php
<?php

use Kukymbr\CurlWrapper\Curl;

// Create Curl instance
$curl = new Curl();

// Set handler options
$curl->setOptArray(
    [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FAILONERROR => true,
        // ... any others CURLOPT_* options
    ]
);

// Request the "https://example.com?foo-bar" URL
$response = $curl->exec('https://example.com', ['foo' => 'bar']);

// Get the response code
$responseCode = $curl->getResponseCode();

```

All of curl_* functions are available in lowerCamelCase, 
for example `$curl->fileCreate()` will cause a call of `curl_file_create` function.

### Using with default values

If you need set your own default curl options,
extend Curl and override the `_getDefaultOptions` method:

```php
<?php

class MyCurl extends \Kukymbr\CurlWrapper\Curl
{
    /**
     * Get default cURL options.
     *
     * @return array
     */
    protected function _getDefaultOptions() : array
    {
        return [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FAILONERROR => true,
        ];
    }
}
```

## License

The MIT License (MIT). Please see License File for more information.